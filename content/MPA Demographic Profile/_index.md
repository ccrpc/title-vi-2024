---
title: Demographic Analysis
draft: false
menu: main
weight: 20
---

The demographic analysis portion of the document includes a summary of the data sources and methodology used, a look at the demographic make-up of the region, and a section dedicated to the analysis of access for protected populations and disadvantaged areas.