---
title: Introduction
draft: false
menu: main
weight: 10
---

The introduction provides historical background on the Title VI program, protected demographic classes, important federal legislation, and the roles and requirements of the Champaign-Urbana Urbanized Area Transportation Study (CUUATS).



## Title VI of the Civil Rights Act of 1964 and Other Legislation

Title VI of the Civil Rights Act of 1964, prohibits discrimination based upon race, color, and national origin.  Specifically, 42 USC 2000d states that “No person in the United States shall, on the ground of race, color, or national origin, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any program or activity receiving Federal financial assistance.”  The use of the word “person” is important as the protections afforded under Title VI apply to anyone, regardless of whether the individual is lawfully present in the United States or a citizen of a State within the United States.

In addition to Title VI, there are other Nondiscrimination statutes that afford legal protection.  These statutes include the following: Section 162 (a) of the Federal-Aid Highway Act of 1973 (23 USC 324) (sex), the Age Discrimination Act of 1975 (age), and Section 504 of the Rehabilitation Act of 1973/Americans With Disabilities Act of 1990 (disability). <sup><a href="#fn1" id="ref1">1</a></sup>

Taken together, these requirements define an over-arching Title VI/Nondiscrimination Program. It is important to also understand that Title VI and the additional Nondiscrimination requirements are applicable to Federal Programs in addition to programs receiving federal financial assistance due to the Civil Rights Restoration Act of 1987. 

## Requirement for Preparing a Title VI Program

<a href="https://www.ecfr.gov/current/title-49/subtitle-A/part-21/section-21.9">Title 49 CFR Section 21.9(b)</a> requires recipients to submit reports to Federal Transit Administration (FTA) in order for FTA to ascertain whether the recipient is in compliance with the Department of Transportation (DOT) Title VI regulations. Recipients must have available “racial and ethnic data showing the extent to which members of minority groups are beneficiaries of programs receiving Federal financial assistance.”

This report covers the period from January 1, 2019 through December 31, 2023. It includes the following information specified in <a href="https://www.transit.dot.gov/regulations-and-guidance/fta-circular-47021b-title-6-requirements-and-guidelines-federal-transit">FTA Circular 4702.1B</a>, Chapter III, Section 4 (Requirements to Prepare and Submit a Title VI Program) and Chapter VI (Requirements for MPOs):

<ul>
<li> A copy of the CCRPC Title VI notice to the public that indicates the recipient complies with Title VI, and informs members of the public of the protections against discrimination afforded to them by Title VI. </li>
<li> A copy of the CCRPC instructions to the public regarding how to file a Title VI discrimination complaint, including a copy of the complaint form. </li>
<li> A list of any public transportation-related Title VI investigations, complaints, or lawsuits filed with CCRPC since the time of the last submission. This list should include only those investigations, complaints, or lawsuits that pertain to allegations of discrimination on the basis of race, color, and/or national origin in transit-related activities and programs and that pertain to CCRPC, not necessarily the larger agency or department of which the recipient is a part. </li>
<li> A public participation plan that includes an outreach plan to engage minority and limited English proficient populations, as well as a summary of outreach efforts made since the last Title VI Program submission. A targeted public participation plan for minority populations may be part of efforts that extend more broadly to include other constituencies that are traditionally under-served, such as people with disabilities, low-income populations, and others. </li>
<li> A copy of the CUUATS/CCRPC plan for providing language assistance to persons with limited English proficiency, based on the DOT LEP Guidance. </li>
<li> A demographic profile of the metropolitan area that includes identification of the locations of minority populations in the aggregate. </li>
<li> A description of the procedures by which the mobility needs of minority populations are identified and considered within the planning process. </li>
<li> Demographic maps that overlay the percent minority and non-minority populations as identified by Census or ACS data, at Census tract or block group level, and charts that analyze the impacts of the distribution of State and Federal funds in the aggregate for public transportation purposes, including Federal funds managed by the MPO as a designated recipient. </li>
<li> An analysis of impacts identified in the preceding paragraph that identifies any disparate impacts on the basis of race, color, or national origin, and, if so, determines whether there is a substantial legitimate justification for the policy that resulted in the disparate impacts, and if there are alternatives that could be employed that would have a less discriminatory impact. </li>
</ul>

## Justice40 and the Bipartisan Infrastructure Law

The Justice 40 Initiative was created under <a href="https://www.whitehouse.gov/briefing-room/presidential-actions/2021/01/27/executive-order-on-tackling-the-climate-crisis-at-home-and-abroad/">Executive Order 14008</a><i> Tackling the Climate Crisis at Home and Abroad </i>, signed in January 2021. It emphasizes that all federally funded programs increase their efforts to benefit disadvantaged communities toward a goal of 40-percent of federal investment flow to disadvantaged communities. Focused investment areas under Justice 40 are climate change, clean energy and energy efficiency, clean transit, affordable and sustainable housing, training and workforce development, remediation and reduction of legacy pollution, and the development of critical clean water and wastewater infrastructure. 

The Bipartisan Infrastructure Law (BIL) was signed by President Biden on November 15, 2021. The transportation investments from the BIL aim to "rebuild America’s roads, bridges and rails, expand access to clean drinking water, ensure every American has access to high-speed internet, tackle the climate crisis, advance environmental justice, and invest in communities that have too often been left behind."<sup><a href="#fn2" id="ref2">1</a></sup>

The role for CUUATS in Justice 40 stems from its linkages to the US Department of Transportation under the Federal Highway Administration (FHWA) and Federal Transit Administration (FTA). CUUATS has used tools to expand on its 2024 Title VI Program to better identify potential underserved populations in transportation and ensure meaningful public participation occurs in the planning processes.

## CUUATS Background

The transportation planning process began with Congressional approval of the Federal-Aid Highway Act on October 23, 1962. This legislation requires that in urban areas, programs for Federal-Aid Highway projects approved after July 1, 1965, must be based on a “...<i> continuing </i> and <i> comprehensive </i> transportation planning process carried on <i> cooperatively </i> by states and local communities.” This required “three-C” planning process established the basis for metropolitan transportation planning used today.

As a result of the 1962 Act, an official body, the Champaign-Urbana Urbanized Area Transportation Study (CUUATS) was created in 1964 to build upon this planning effort and administer the area’s three-C transportation planning process. In order to ensure that comprehensive regional planning efforts were achieved, the Governor of the State of Illinois designated the Champaign County Regional Planning Commission (CCRPC) as the Metropolitan Planning Organization (MPO) in March 1974.

CCRPC administers CUUATS staff, and to avoid any duplication of effort, it has recognized CUUATS as its own transportation planning entity. CUUATS member agencies are the Cities of Champaign and Urbana, the Village of Savoy, Champaign County, the University of Illinois, Champaign-Urbana Mass Transit District, Champaign County Regional Planning Commission, and the Illinois Department of Transportation (IDOT), all of which are voting members. The Village of Bondville is included in the 2020 Census-defined urban area, which makes it eligible to be a CUUATS member agency, though they do not participate. The voting members are part of the CUUATS Policy Committee, the official decision-making body of the Metropolitan Planning Organization (MPO).

In March 1979, a Memorandum of Agreement was signed by all member agencies of CUUATS, CCRPC and IDOT “...for the purpose of continuing and documenting the framework of a continuing, cooperative and comprehensive transportation planning process that results in plans and programs consistent with the comprehensively planned development of the Champaign-Urbana urbanized area.” An updated Memorandum of Agreement was endorsed by all participating CUUATS member agencies in June 2011. This document delineates responsibilities and actions between CUUATS and the CCRPC.

The MPO is responsible for the following aspects of the transportation planning process:

<ul>
<li>To give advice regarding development in the study area;</li>
<li>To review and advise on proposed changes in transportation planning concepts;</li>
<li>To serve as liaison between governmental units in the study area;</li>
<li>To obtain optimum cooperation of all governmental units in providing information and in implementing various elements of the transportation plan.</li>
<li>To design and set goals and objectives of the planning process and the long range transportation plan;</li>
<li>To produce an annual Unified Planning Work Program (UPWP) listing the transportation planning activities CUUATS staff will undertake in the upcoming fiscal year from July 1 through June 30 of the following year.</li>
<li>To produce and maintain an annual Transportation Improvement Program (TIP) listing anticipated transportation projects for the next four fiscal years for all CUUATS member agencies.</li>
<li>To produce a Long Range Transportation Plan (LRTP) every five years outlining how the Urban Area transportation system should evolve over the next 25 years.</li>
</ul>

### CUUATS Organizational Structure

CUUATS operates under the principal direction of two committees - the Policy Committee and the Technical Committee. In its organizational structure, the Technical Committee acts as a working committee under the direction of the Policy Committee.

#### CUUATS Policy Committee

The CUUATS Policy Committee consists of local elected and appointed officials of those agencies that have a primary interest in transportation. Each member is expected to reflect the official position of his or her constituent agency and/or the public interests they represent. This committee assumes the decision-making authority for CUUATS and establishes policies that guide and form the transportation planning process. The committee has a variety of responsibilities, which range from approving the annual Transportation Improvement Program (TIP) to helping establish the metropolitan planning boundary of the study area and other federal documents prepared by CUUATS staff. CUUATS Policy Committee membership is currently composed of representatives of the following agencies:

<ul>
<li>Mayor, City of Champaign </li>
<li>Mayor, City of Urbana</li>
<li>President, Village of Savoy</li>
<li>Chair, Champaign County Board</li>
<li>Chair, Champaign-Urbana Mass Transit District Board of Trustees</li>
<li>Program Development Engineer, Illinois Department of Transportation - District 5</li>
<li>Executive Director, University of Illinois Facilities and Services</li>
</ul>

#### CUUATS Technical Committee

The CUUATS Technical Committee consists of staff from all CUUATS participating agencies. It performs analyses and makes recommendations concerning transportation issues to the Policy Committee for their approval. The actual technical work is performed by CUUATS staff and the CUUATS Technical Committee member organizations. CUUATS Technical Committee membership is currently composed of representatives of the following agencies:

<ul>
<li>County of Champaign (2 voting members): County Engineer, Assistant County Engineer </li>
<li>City of Champaign (2 voting members): City Engineer, Assistant Planning Director</li>
<li>City of Urbana (2 voting members): City Engineer, Principal Planning and Zoning Administrator</li>
<li>Village of Savoy (2 voting members): Director of Public Works, Community Development Director</li>
<li>University of Illinois (2 voting members): Director of Planning, Transportation Systems Manager</li>
<li>Mass Transit District (2 voting member): Managing Director, Chief of Staff </li>
<li>Regional Planning Commission (1 voting member): Chief Executive Officer </li>
<li>State of Illinois (2 voting members): Planning and Services Chief (District 5), Federal Aid Coordinator (District 5)</li>
<li>Technical Advisors (non-voting): FHWA Division Office; IDOT Central Bureau of Urban Program Planning; IDOT Office of Intermodal Project Implementation; FHWA Division Office;Airport, Rail, and Freight Representatives; Village Representatives (Mahomet, Tolono, Bondville); Township Representatives (Somer, Hensley, Champaign, Urbana)</li>
</ul>

#### CUUATS Staff

The professional time and services for transportation planning in this cooperative effort includes CCRPC staff assigned to CUUATS as well as staff from IDOT and other member agencies. CUUATS staff perform the day-to-day operations of the agency, with technical expertise in transportation systems planning and other related areas. The staff, in conjunction with CUUATS member agencies, collect, analyze, and evaluate demographic, land use, and transportation data to determine the transportation system requirements of the urban area or the metropolitan planning area, depending on the requirements. They also prepare materials for use at Technical and Policy Committee meetings and other meetings, as well as for any existing subcommittees. Staff members participate in all CUUATS meetings and provide expertise as needed. In addition, they represent the agency at other meetings of importance to planning activities within the region. The current CUUATS staff positions are as follows:

<ul>
<li>Planning and Community Development Director</li>
<li>Planning Manager</li>
<li>Data and Technology Manager</li>
<li>Research Analyst</li>
<li>Transportation Engineer</li>
<li>Transportation Planners II and III</li>
<li>GIS Analyst</li>
<li>Executive Assistant</li>
</ul>

<hr>

<sup id="fn1">1. <a href="https://highways.dot.gov/civil-rights/title-vi-civil-rights-act-1964-and-additional-nondiscrimination-requirements">Title VI of The Civil Rights Act of 1964 and Additional Nondiscrimination Requirements.</a>US Department of Transportation, FHWA.<a href="#ref1" title="Jump back to footnote 1 in the text.">↩</a></sup>

<sup id="fn2">2. <a href="https://www.whitehouse.gov/briefing-room/statements-releases/2021/11/06/fact-sheet-the-bipartisan-infrastructure-deal/">Fact Sheet: The Bipartisan Infrastructure Deal.</a>The White House.<a href="#ref2" title="Jump back to footnote 1 in the text.">↩</a></sup>