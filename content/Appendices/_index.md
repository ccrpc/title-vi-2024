---
title: Appendices
draft: false
menu: main
weight: 60
---

The appendices provide legal documentation of the responsibilities of the Metropolitan Planning Organization (CUUATS) under various circumstances related to its work under the Title VI program's requirements for non-discrimination.