---
title: Appendix III
draft: false
weight: 53
---

## CLAUSES FOR TRANSFER OF REAL PROPERTY ACQUIRED OR IMPROVED UNDER THE ACTIVITY, FACILITY, OR PROGRAM

The following clauses will be included in deeds, licenses, leases, permits, or similar instruments entered into by the <b>CCRPC</b> pursuant to the provisions of Assurance 7(a):

A. The (grantee, lessee, permittee, etc. as appropriate) for himself/herself, his/her heirs, personal representatives, successors 	in interest, and assigns, as a part of the consideration hereof, does hereby covenant and agree [in the case of deeds and leases add “as a covenant running with the land”] that:

<ol>
<li>In the event facilities are constructed, maintained, or otherwise operated on the property described in this (deed, license, lease, permit, etc.) for a purpose for which a U.S. Department of Transportation activity, facility, or program is extended or for another purpose involving the provision of similar services or benefits, the (grantee, licensee, lessee, permittee, etc.) will maintain and operate such facilities and services in compliance with all requirements imposed by the Acts and Regulations (as may be amended) such that no person on the grounds of race, color, or national origin, will be excluded from participation in, denied the benefits of, or be otherwise subjected to discrimination in the use of said facilities.</li>
</ol>

B. With respect to licenses, leases, permits, etc., in the event of breach of any of the above Non-discrimination covenants, <b>CCRPC</b> will have the right to terminate the (lease, license, permit, etc.) and to enter, re-enter, and repossess said lands and facilities thereon, and hold the same as if the (lease, license, permit, etc.) had never been made or issued.*

C. With respect to a deed, in the event of breach of any of the above Non-discrimination covenants, the <b>CCRPC</b> will have the right to enter or re-enter the lands and facilities thereon, and the above described lands and facilities will there upon revert to and vest in and become the absolute property of the <b>CCRPC</b> and its assigns.*

(*Reverter clause and related language to be used only when it is determined that such a clause is necessary to make clear the purpose of Title VI.) 