---
title: Appendix II
draft: false
weight: 52
---

<center>## CLAUSES FOR DEEDS TRANSFERRING UNITED STATES PROPERTY</center>

The following clauses will be included in deeds effecting or recording the transfer of real property, structures, or improvements thereon, or granting interest therein from the United States pursuant to the provisions of Assurance 4:

NOW THEREFORE, the U.S. Department of Transportation as authorized by law and upon the condition that the <b>CCRPC</b> will accept title to the lands and maintain the project constructed thereon in accordance with <b>the Uniform Administrative Requirement, Cost Principles, and Audit Requirements for Federal Awards (2 C.F.R. Part 200), the regulations for the administration of the Federal Highway Program, and the policies and procedures prescribed by the Federal Highway Administration </b> of the U.S. Department of Transportation in accordance and in compliance with all requirements imposed by Title 49, Code of Federal Regulations, U.S. Department of Transportation, Subtitle A, Office of the Secretary, Part 21, Non-discrimination in Federally-assisted programs of the U.S. Department of Transportation pertaining to and effectuating the provisions of Title VI of the Civil Rights Act of 1964 (78 Stat. 252; 42 U.S.C. § 2000d to 2000d-4), does hereby remise, release, quitclaim, and convey unto the <b>CCRPC</b> all the right, title and interest of the U.S. Department of Transportation in and to said lands described in Exhibit A attached hereto and made a part hereof.

(HABENDUM CLAUSE)

TO HAVE AND TO HOLD said lands and interests therein unto<b> CCRPC </b>and its successors forever, subject, however, to the covenants, conditions, restrictions and reservations herein contained as follows, which will remain in effect for the period during which the real property or structures are used for a purpose for which Federal financial assistance is extended or for another purpose involving the provision of similar services or benefits and will be binding on the<b> CCRPC</b>, its successors and assigns.

The<b> CCRPC </b>, in consideration of the conveyance of said lands and interest in lands, does hereby covenant and agree as a covenant running with the land for itself, its successors and assigns, that (1) no person will on the grounds of race, color, or national origin, be excluded from participation in, be denied the benefits of, or be otherwise subjected to discrimination with regard to any facility located wholly or in part on, over, or under such lands hereby conveyed [,] [and]* (2) that the <b>CCRPC<b> will use the lands and interests in lands and interest in lands so conveyed, in compliance with all requirements imposed by or pursuant to Title 49, Code of Federal Regulations, U.S. Department of Transportation, Subtitle A, Office of the Secretary, Part 21, Non-discrimination in Federally-assisted programs of the U.S. Department of Transportation, Effectuation of Title VI of the Civil Rights Act of 1964, and as said Regulations and Acts may be amended[, and (3) that in the event of breach of any of the above-mentioned non-discrimination conditions, the Department will have a right to enter or re-enter said lands and facilities on said land, and that above described land and facilities will thereon revert to and vest in and become the absolute property of the U.S. Department of Transportation and its assigns as such interest existed prior to this instruction].*

(*Reverter clause and related language to be used only when it is determined that such a clause is necessary in order to make clear the purpose of Title VI.)
