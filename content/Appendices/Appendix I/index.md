---
title: Appendix I
draft: false
weight: 51
---

During the performance of this contract, the contractor, for itself, its assignees, and successors in interest (hereinafter referred to as the “contractor”) agrees as follows:

<ol>
<li><b>Compliance with Regulations:</b> The contractor (hereinafter includes consultants) will comply with the Acts and the Regulations relative to Non-discrimination in Federally-assisted programs of the U.S. Department of Transportation, <b>Federal Highway Administration (FHWA)</b>, as they may be amended from time to time, which are herein incorporated by reference and made a part of this contract.</li>

<li><b>Non-discrimination:</b> The contractor, with regard to the work performed by it during the contract, will not discriminate on the grounds of race, color, national origin, sex, age, or disability in the selection and retention of subcontractors, including procurements of materials and leases of equipment. The contractor will not participate directly or indirectly in the discrimination prohibited by the Acts and the Regulations, including employment practices when the contract covers any activity, project, or program set forth in Appendix B of 49 CFR Part 21.</li>

<li><b>Solicitations for Subcontracts, Including Procurements of Materials and Equipment:</b> In all solicitations, either by competitive bidding, or negotiation made by the contractor for work to be performed under a subcontract, including procurements of materials, or leases of equipment, each potential subcontractor or supplier will be notified by the contractor of the contractor’s obligations under this contract and the Acts and the Regulations relative to Non-discrimination on the grounds of race, color, or national origin.</li>

<li><b>Information and Reports:</b> The contractor will provide all information and reports required by the Acts, the Regulations, and directives issued pursuant thereto and will permit access to its books, records, accounts, other sources of information, and its facilities as may be determined by the Recipient or the <b>FHWA</b> to be pertinent to ascertain compliance with such Acts, Regulations, and instructions. Where any information required of a contractor is in the exclusive possession of another who fails or refuses to furnish the information, the contractor will so certify to the Recipient or the <b>FHWA</b>, as appropriate, and will set forth what efforts it has made to obtain the information.</li>

<li><b>Sanctions for Noncompliance:</b> In the event of a contractor’s noncompliance with the Non-discrimination provisions of this contract, the Recipient will impose such contract sanctions as it or the <b>FHWA</b> may determine to be appropriate, including, but not limited to 

<blockquote>a. withholding payments to the contractor under the contract until the contractor complies; and/or

b. cancelling, terminating, or suspending a contract, in whole or in part.</blockquote></li>

<li><b>Incorporation of Provisions:</b> The contractor will include the provisions of paragraphs one through six in every subcontract, including procurements of materials and leases of equipment, unless exempt by the Acts, the Regulations and directives issued pursuant thereto. The contractor will take action with respect to any subcontract or procurement as the Recipient or the <b>FHWA</b>. A may direct as a means of enforcing such provisions including sanctions for noncompliance. Provided, that if the contractor becomes involved in, or threatened with litigation by a subcontractor, or supplier because of such direction, the contractor may request the Recipient to enter into any litigation to protect the interests of the Recipient. In addition, the contractor may request the United States to enter into the litigation to protect the interests of the United States.</li>
</ol>