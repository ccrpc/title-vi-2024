---
title: Appendix V
draft: false
weight: 55
---

During the performance of this contract, the contractor, for itself, its assignees, and succors in interest (hereinafter referred to as the “contractor”) agrees to comply with the following non-discrimination statutes and authorities, including, but not limited to:

## Pertinent Non-Discrimination Authorities:

<ul>
<li>Title VI of the Civil Rights Act of 1964 (42 U.S.C. § 2000d et seq., 78 stat.252), prohibits discrimination on the basis of race, color, national origin); and 49 CFR Part 21.</li>
<li>The Uniform Relocation Assistance and Real Property Acquisition Policies Act of 1970, (42 U.S.C. § 4601), (prohibits unfair treatment of persons displaced or whose property has been acquired because of Federal or Federal-aid programs and projects);</li>
<li>Federal-Aid Highway Act of 1973, (23 U.S.C. § 324 et seq.), prohibits discrimination on the basis of sex);</li>
<li>Section 504 of the Rehabilitation Act of 1973, (29 U.S.C. § 794 et seq.), as amended, prohibits discrimination on the basis of disability; and 49 CFR Part 27;</li>
<li>The Age Discrimination Act of 1975, as amended, (42 U.S.C. § 6101 et seq.), prohibits discrimination on the basis of age);
Airport and Airway Improvement Act of 1982, (49 U.S.C. § 471, Section 47123, as amended, (prohibits discrimination based on race, creed, color, national origin, or sex);</li>
<li>The Civil Rights Restoration Act of 1987, (PL 100-209), Broadened the scope, coverage and applicability of Title VI of the Civil Rights Act of 1964, The Age Discrimination Act of 1975 and Section 504 of the Rehabilitation Act of 1973, by expanding the definition of the terms “programs or activities” to include all of the programs or activities of the Federal-aid recipients, sub-recipients and contractors, whether such programs or activities are Federally funded or not);</li>
<li>Titles II and III of the Americans with Disabilities Act, which prohibit discrimination of the basis of disability in the operation of public entities, public and private transportation systems, places of public accommodation, and certain testing entities (42 U.S.C. §§ 12131 – 12189) as implemented by Department of Transportation regulations 49 C.F.R. parts 37 and 38;</li>
<li>The Federal Aviation Administration’s Non-discrimination statute (49 U.S.C. § 47123) (prohibits discrimination on the basis of race, color, national origin, and sex);</li>
<li>Executive Order 12898, Federal Actions to Address Environmental Justice in Minority Populations and Low-Income Populations, which ensures discrimination against minority populations by discouraging programs, policies, and activities with disproportionately high and adverse human health or environmental effects on minority and low-income populations;</li>
<li>Executive Order 13166, Improving Access to Services for Persons with Limited English Proficiency, and resulting agency guidance, national origin discrimination includes discrimination because of limited English proficiency (LEP). To ensure compliance with Title VI, you must take reasonable steps to ensure that LEP persons have meaningful access to your programs (70 Fed. Reg. at 74087 to 74100);</li>
<li>Title IX of the Education Amendments of 1972, as amended, which prohibits you from discriminating because of sex in education programs or activities (20 U.S.C. 1681 et seq).</li>
</ul>
