---
title: Forms
draft: false
weight: 41
---
## TItle VI Notice to the Public

The Champaign County Regional Planning Commission (CCRPC) is the Metropolitan Planning Organization (MPO) responsible for administering the federally mandated transportation process for the Champaign-Urbana urbanized area. CCRPC operates its programs and services without regard to race, color, and national origin in accordance with Title VI of the Civil Rights Act. Any person who believes she or he has been aggrieved by any unlawful discriminatory practice under Title VI may file a complaint with the CCRPC.

For more information on CCRPC’s civil rights program, and the procedures to file a complaint, contact Becky Krueger by phone, (217) 328-3313; email, bkrueger@ccrpc.org; or visit our administrative office at 1776 E Washington St, Urbana, IL 61802. For more information, visit www.ccrpc.org.

A complainant may file a complaint directly with the Federal Transit Administration by filing a complaint with the Office of Civil Rights, Attention: Title VI Program Coordinator, East Building, 5th Floor-TCR, 1200 New Jersey Ave., SE, Washington, DC 20590.

<center>If information is needed in another language, contact (217) 328-3313.

Si necesita información en otro idioma, comuníquese con el (217) 328-3313.

Si des informations sont nécessaires dans une autre langue, contactez (217) 328-3313.

如果在其他语言方面需要更多的信息，请电话联系 (217) 328-3313.

다른 언어로 이정보를 알기 원하시면 (217) 328-3313 으로 연락하시면 됩니다.

Nếu quí vị cần tin tức bằng ngôn ngữ khác, xin liên lạc (217) 328-3313.</center>

## Title VI Complaint Procedure

Any person who believes she or he has been discriminated against on the basis of race, color, or national origin by the Champain-Urbana Urbanized Area Transportation Study (CUUATS), the MPO for the Champaign-Urbana urban area, may file a Title VI complaint by completing and submitting the agency’s Title VI Complaint Form. The Champaign County Regional Planning Commission (CCRPC), the organization that houses CUUATS, investigates complaints received no more than 180 days after the alleged incident. CCRPC will process complaints that are complete.

Once the complaint is received, CCRPC will review it to determine if our office has jurisdiction. The complainant will receive an acknowledgment letter informing her/him whether the complaint will be investigated by our office within 10 business days.
The CCRPC has 60 business days to investigate the complaint. If more information is needed to resolve the case, CCRPC may contact the complainant. The complainant has 10 business days from the date of the letter to send requested information to the investigator assigned to the case. If the investigator is not contacted by the complainant or does not receive the additional information within 10 business days, CCRPC can administratively close the case.

A case can be administratively closed also if the complainant no longer wishes to pursue their case. After the investigator reviews the complaint, she/he will issue one of two letters to the complainant: a closure letter or a letter of finding (LOF). A closure letter summarizes the allegations and states that there was not a Title VI violation and that the case will be closed. An LOF summarizes the allegations and the interviews regarding the alleged incident, and explains whether any disciplinary action, additional training of the staff member, or other action will occur. If the complainant wishes to appeal the decision, she/he has 10 business days after the date of the letter or the LOF to do so.

A person may also file a complaint directly with the Federal Transit Administration, at FTA Office of Civil Rights, 1200 New Jersey Avenue SE, Washington, DC 20590.

<center>If information is needed in another language, contact (217) 328-3313.

Si necesita información en otro idioma, comuníquese con el (217) 328-3313.

Si des informations sont nécessaires dans une autre langue, contactez (217) 328-3313.

如果在其他语言方面需要更多的信息，请电话联系 (217) 328-3313.

다른 언어로 이정보를 알기 원하시면 (217) 328-3313 으로 연락하시면 됩니다.

Nếu quí vị cần tin tức bằng ngôn ngữ khác, xin liên lạc (217) 328-3313.</center>

## Title VI Complaint Form

To download a copy of the Title VI Complaint Form, <a href="https://cms3.revize.com/revize/champaigncountyrpc/Title%20VI%20Complaint%20Form.pdf">click here</a>.

## List of Transit-Related Title VI Investigations, Complaints, and Lawsuits

All recipients shall prepare and maintain a list of any of the following that allege discrimination on the basis of race, color, or national origin:

<ul>
<li>Active investigations conducted by FTA and entities other than FTA;</li>
<li>Lawsuits; and</li>
<li>Complaints naming the recipient.</li>
</ul>

This list shall include the date that the transit-related Title VI investigation, lawsuit, or complaint was filed; a summary of the allegation(s); the status of the investigation, lawsuit, or complaint; and actions taken by the recipient in response, or final findings related to the investigation, lawsuit, or complaint. This list shall be included in the Title VI Program submitted to FTA every three years.

There have been no Title VI investigations, complaints, or lawsuits filed against CCRPC/CUUATS for the current Title VI reporting period from January 1, 2019 through December 31, 2023.  

<rpc-table 
url="Title VI Investigations, Complaints.csv" 
text-alignment="1,r" 
table-title="List of Transit-Related Title VI Investigations, Complaints, and Lawsuits" 
switch="false">
</rpc-table>