---
title: Title VI Assurances and Forms
draft: false
menu: main
weight: 50
---

The assurances and forms section provides a Title VI Notice to the Public, assurances regarding protections against discriminatory practices, a link to a downloadable version of the Title Complaint Form, and a list of any Title VI investigations since the previous Title VI document was adopted (there were none).