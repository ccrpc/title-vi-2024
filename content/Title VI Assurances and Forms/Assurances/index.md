---
title: Assurances
draft: false
weight: 42
---
(A signed PDF copy of this document will be added to the CCRPC website upon final approval of this document.)

<center>

### United States Department of Transportation (USDOT)

### Standard Title VI/Nondiscrimination Assurances

### <a href="https://www.bing.com/ck/a?!&&p=14d7cd8ecc2baf4fJmltdHM9MTcxMTU4NDAwMCZpZ3VpZD0yMjRjZDVhMi0wZTc0LTZiYjUtMWM1My1jNmQ0MGZhNzZhZjcmaW5zaWQ9NTIwOA&ptn=3&ver=2&hsh=3&fclid=224cd5a2-0e74-6bb5-1c53-c6d40fa76af7&psq=DOT+Order+No.+1050.2A&u=a1aHR0cHM6Ly93d3cuZmh3YS5kb3QuZ292L2NpdmlscmlnaHRzL3Byb2dyYW1zL3RpdGxlX3ZpL1VTRE9UX09yZGVyXzA1MDJBLnBkZg&ntb=1">DOT Order No. 1050.2A </a> </center>

The Champaign County Regional Planning Commission hereinafter referred to as CCRPC (herein referred to as the “Recipient”), HEREBY AGREES THAT, as a condition to receiving any Federal financial assistance from the U.S. Department of Transportation (DOT), through the <u> Federal Highway Administration (FHWA) </u>, is subject to and will comply with the following: 

## Statutory/Regulatory Authorities

<ul>
<li>Title VI of the Civil Rights Act of 1964 (42 U.S.C. § 2000d et seq., 78 stat. 252), (prohibits discrimination on the basis of race, color, national origin);</li>
<li>49 C.F.R. Part 21 (entitled Nondiscrimination In Federally-Assisted Programs Of The Department Of Transportation—Effectuation Of Title VI Of The Civil Rights Act Of 1964);</li>
<li>28 C.F.R. § 50.3 (U.S. Department of Justice Guidelines for Enforcement of Title VI of the Civil Rights Act of 1964);</li>
<li>The Uniform Relocation Assistance and Real Property Acquisition Policies Act of 1970, (<a href="https://uscode.house.gov/view.xhtml?path=/prelim@title42/chapter61&edition=prelim">42 U.S.C. § 4601 <i>et seq.</i></a>), prohibits unfair treatment of persons displaced or whose property has been acquired because of Federal or Federal-aid programs and projects;</li>
<li>Section 504 of the Rehabilitation Act of 1973, as amended,(<a href="https://www.ecfr.gov/current/title-49/subtitle-A/part-27">29 U.S.C. § 794 <i>et seq.</i></a>)prohibits discrimination on the basis of disability; and <a href="https://www.ecfr.gov/current/title-49/subtitle-A/part-27">49 CFR Part 27</a>;</li>
<li>The Age Discrimination Act of 1975, as amended, (<a href="https://www.govinfo.gov/content/pkg/USCODE-2022-title42/pdf/USCODE-2022-title42-chap76-sec6101.pdf">42 U.S.C. § 6101 <i>et seq.</i></a>), prohibits discrimination on the basis of age;</li>
<li>The <a href="https://www.congress.gov/bill/100th-congress/senate-bill/557/text">Civil Rights Restoration Act of 1987, (PL 100-259)</a>, (Broadened the scope, coverage and applicability of Title VI of the Civil Rights Act of 1964, The Age Discrimination Act of 1975 and Section 504 of the Rehabilitation Act of 1973, by expanding the definition of the terms “programs or activities” to include all of the programs or activities of the Federal-aid recipients, sub-recipients and contractors, whether such programs or activities are Federally funded or not);</li>
<li>Title II and III of the Americans with Disabilities Act, which prohibit discrimination on the basis of disability in the operation of public entities, public and private transportation systems, places of public accommodation, and certain testing entities (<a href="https://uscode.house.gov/view.xhtml?req=granuleid%3AUSC-prelim-title42-chapter126-subchapter2&saved=%7CKHRpdGxlOjQyIHNlY3Rpb246MTIxMDEgZWRpdGlvbjpwcmVsaW0p%7C%7C%7C0%7Cfalse%7Cprelim&edition=prelim">42 U.S.C. §§ 12131 – 12189</a>) as implemented by Department of Transportation regulations <a href="https://www.ecfr.gov/current/title-49/subtitle-A">49 C.F.R. parts 37 and 38</a>;</li>
<li>Title IX of the Education Amendments of 1972, as amended, which prohibits you from discriminating because of sex in education programs or activities (<a href="https://uscode.house.gov/view.xhtml?path=/prelim%40title20/chapter38&edition=prelim">20 U.S.C. 1681 et seq</a>).</li>
</ul>

The preceding statutory and regulatory cites hereinafter are referred to as the “Acts” and “Regulations,” respectively.
## General Assurances

In accordance with the Acts, the Regulations, and other pertinent directives, circulars, policy, memoranda, and/or guidance, the Recipient hereby gives assurance that it will promptly take any measures necessary to ensure that:

<blockquote>“No person in the United States shall, on the grounds of race, color, or national origin, be excluded from participation in, be denied the benefits of, or be otherwise subjected to discrimination under any program or activity,” for which the Recipient receives Federal financial assistance from DOT, including the <u> Federal Highway Administration (FHWA) </u>."</blockquote>

The Civil Rights Restoration Act of 1987 clarified the original intent of Congress, with respect to Title VI and other Nondiscrimination requirements (The Age Discrimination Act of 1975, and Section 504 of the Rehabilitation Act of 1973), by restoring the broad, institutional-wide scope and coverage of these nondiscrimination statutes and requirements to include all programs and activities of the Recipient, so long as any portion of the program is Federally assisted.

## Specific Assurances

More specifically, and without limiting the above general Assurance, the Recipient agrees with and gives the following Assurances with respect to its <u>federally assisted programs under the Champaign-Urbana Urbanized Area Transportation Study metropolitan planning organization</u>:

<ol>
<li>The Recipient agrees that each “activity,” “facility,” or “program,” as defined in §§ 21.23 (b) and 21.23 (e) of 49 C.F.R. § 21 will be (with regard to an “activity”) facilitated, or will be (with regard to a “facility”) operated, or will be (with regard to a “program”) conducted in compliance with all requirements imposed by, or pursuant to the Acts and the Regulations.</li>
<li>The Recipient will insert the following notification in all solicitations for bids, Requests For Proposals for work, or material subject to the Acts and the Regulations made in connection with all <u>Federal Highway Programs</u> and, in adapted form, in all proposals for negotiated agreements regardless of funding source:
<blockquote> “The  <u>  CCRPC  </u>  in accordance with the provisions of Title VI of the Civil Rights Act of 1964 (78 Stat. 252, 42 U.S.C. §§ 2000d to 2000d-4) and the Regulations, hereby notifies all bidders that it will affirmatively insure that any contract entered into pursuant to this advertisement, disadvantaged business enterprises will be afforded full opportunity to submit bids in response to this invitation and will not be discriminated against on the grounds of race, color, or national origin in consideration for an award.”</blockquote></li>
<li>The Recipient will insert the clauses of Appendix A and E of this Assurance in every contract or agreement subject to the Acts and the Regulations.</li>
<li>The Recipient will insert the clauses of Appendix B of this Assurance, as a covenant running with the land, in any deed from the United States effecting or recording a transfer of real property, structures, use, or improvements thereon or interest therein to a Recipient</li>
<li>That where the Recipient receives Federal financial assistance to construct a facility, or part of a facility, the Assurance will extend to the entire facility and facilities operated in connection therewith.</li>
<li>That where the Recipient receives Federal financial assistance in the form, or for the acquisition of real property or an interest in real property, the Assurance will extend to rights to space on, over, or under such property.</li>
<li>That the Recipient will include the clauses set forth in Appendix C and Appendix D of this Assurance, as a covenant running with the land, in any future deeds, leases, licenses, permits, or similar instruments entered into by the Recipient with other parties:
<blockquote>a. for the subsequent transfer of real property acquired or improved under the applicable activity, project, or program; and

b. for the construction or use of, or access to, space on, over, or under real property acquired or improved under the applicable activity, project, or program.</blockquote></li>
<li>That this Assurance obligates the Recipient for the period during which Federal financial assistance is extended to the program, except where the Federal financial assistance is to provide, or is in the form of, personal property, or real property, or interest therein, or structures or improvements thereon, in which case the Assurance obligates the Recipient, or any transferee for the longer of the following periods:
<blockquote>a. the period during which the property is used for a purpose for which the Federal financial assistance is extended, or for another purpose involving the provision of similar services or benefits; or

b. the period during which the Recipient retains ownership or possession of the property.</blockquote></li>
<li>The Recipient will provide for such methods of administration for the program as are found by the Secretary of Transportation or the official to whom he/she delegates specific authority to give reasonable guarantee that it, other recipients, sub-recipients, sub-grantees, contractors, subcontractors, consultants, transferees, successors in interest, and other participants of Federal financial assistance under such program will comply with all requirements imposed or pursuant to the Acts, the Regulations, and this Assurance.</li>
<li>The Recipient agrees that the United States has a right to seek judicial enforcement with regard to any matter arising under the Acts, the Regulations, and this Assurance.</li>
</ol>

By signing this ASSURANCE,  <u> CCRPC </u>  also agrees to comply (and require any sub-recipients, sub-grantees, contractors, successors, transferees, and/or assignees to comply) with all applicable provisions governing the <u> FHWA </u> access to records, accounts, documents, information, facilities, and staff. You also recognize that you must comply with any program or compliance reviews, and/or complaint investigations conducted by <u> FHWA </u>. You must keep records, reports, and submit the material for review upon request to <u> FHWA </u> or its designee in a timely, complete, and accurate way. Additionally, you must comply with all other reporting, data collection, and evaluation requirements, as prescribed by law or detailed in program guidance.

<u>    CCRPC  </u>   gives this ASSURANCE in consideration of and for obtaining any Federal grants, loans, contracts, agreements, property, and/or discounts, or other Federal-aid and Federal financial assistance extended after the date hereof to the recipients by the U.S. Department of Transportation under the <u> Federal Highway Program </u>. This ASSURANCE is binding on  <u>  CCRPC </u>   other recipients, sub-recipients, sub-grantees, contractors, subcontractors and their subcontractors’, transferees, successors in interest, and any other participants in the <u> Federal Highway Program </u>. The person(s) signing below is authorized to sign this ASSURANCE on behalf of the Recipient.

Signed____________________________________________________   Dated_______________________


Dr. Dalitso Sulamoyo, CEO, Champaign County Regional Planning Commission 

(A signed PDF copy of this document will be added to the CCRPC website upon final approval of this document.)