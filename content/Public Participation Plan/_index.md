---
title: Link to Public Participation Plan
draft: false
menu: main
weight: 30
---


The Public Participation Plan (PPP) can be found on the <a href="https://ccrpc.org/transportation/public_participation_plan_(ppp)/2023_public_participation_plan_ppp.php">CCRPC website</a>

As part of the TItle VI documentation, the PPP provides the explanations and strategies for CUUATS public outreach to ensure equitable input from the public.