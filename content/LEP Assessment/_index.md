---
title: Limited English Proficiency (LEP) Assessment
draft: false
menu: main
weight: 40
---

The Limited English Proficiency Assessment covers the Title VI requirement to supply a plan for providing language assistance to persons with limited English proficiency, based on the <a href="https://www.transportation.gov/civil-rights/civil-rights-awareness-enforcement/dots-lep-guidance">DOT LEP Guidance</a>. CCRPC/CUUATS follows the U.S. Department of Transportation’s Policy Guidance Concerning Recipients’ Responsibilities to Limited English Proficient (LEP) Persons issued on December 14, 2005. In accordance with DOT Guidelines, CCRPC/CUUATS will use the four-factor analysis that requires an individual assessment for each project.

### <u>Four Factor Analysis</u>

<b>Factor 1: The number or proportion of LEP persons eligible to be served or likely to be encountered by a program, activity, or service of the recipient or grantee.</b>

LEP persons are defined here as those who self-identify as speaking English less than “very well” on the American Community Survey (ACS) form. Other Asian and Pacific Islander language speakers have emerged as the largest proportion of LEP residents since the previous Title VI report in 2019.

According to the U.S. Census Bureau, the primary language in the MPA is English, with 92.7 percent of the population able to speak English. The second most common language spoken is other Asian and Pacific Islander languages, followed by other Indo-European languages, then Spanish, then Other Languages. Table 3 indicates 7.3 percent of the MPA population speaks English less than “very well.”




<center>
<rpc-table 
url="LEP_Eng_Non-Eng.csv" 
text-alignment="1,r"
table-title="Table 19. English- and Non-English Speaking Residents in the MPA, 2022 5-year estimates" 
switch="false" 
source-url="https://data.census.gov/table?q=B16004&g=400XX00US15211,53416" 
source="US Census Bureau 2018-2022 ACS 5-Year Estimates, Table B16004"
style="width:60%"

></center>
</rpc-table>

<center>
<rpc-table 
url="LEP_Assess.csv" 
text-alignment="1,r"
table-title="Table 20. Languages Spoken in the MPA, 2022 5-year estimates" 
switch="false" 
source-url="https://data.census.gov/table?q=B16004&g=400XX00US15211,53416" 
source="US Census Bureau 2018-2022 ACS 5-Year Estimates, Table B16004"
style="width:60%"></center>
</rpc-table>

The number of languages spoken and the number of LEP persons in the MPA regularly fluctuates because of the large number of foreign students studying at The University of Illinois (UIUC)(over 12,500 in <a href="https://dmi.illinois.edu/stuenr/#foreign">Fall 2023</a>). It is important to note that many residents in the community that could be considered LEP live in Orchard Downs, a UIUC housing development for families of graduate students. Orchard Downs is well-connected to the Champaign-Urbana urbanized area via several bus lines. In addition, the university offers language resources for university-affiliated LEP residents through <a href="https://isss.illinois.edu/"> International Student and Scholar Services </a>, the <a href="https://isss.illinois.edu/training-programs/ihc/index.html"> International Hospitality Committee </a>, and the ESL program's list of <a href="https://linguistics.illinois.edu/languages/english-second-language/language-learning-resources/local-english-language-opportunities">Local English Language Opportunities</a>, to name a few.

<b> Factor 2: The frequency with which LEP individuals come in contact with the MPO.</b>

CCRPC/CUUATS has not received formal requests by LEP individuals for language translation or for an interpreter in the current reporting period. CCRPC/CUUATS has bilingual staff members and makes it a priority to make outreach materials (surveys, comment cards, informational videos, websites, etc.) accessible for Spanish, French, and Mandarin speakers as well as for the hearing impaired whenever possible.

<b>Factor 3: The nature and importance of the MPO, activity, or service provided by the MPO to the LEP community.</b>

The MPO does not conduct activities which require residents to fill out applications or submit to interviews prior to attending public functions. The MPO uses Federal funds to plan for transportation projects and therefore does not include any direct service or program that requires vital, immediate, or emergency assistance, such as medical treatment or services for basic needs (like food or shelter). Although CCRPC/CUUATS does not directly provide transportation services, it has aided MTD in transit planning. MTD produces separate Title VI and LEP documentation.

<b> Factor 4: The resources available to the MPO and overall costs.</b>

The final factor weighs the previous factors to assess the needs of LEP individuals against the resources available to the MPO for providing assistance in languages other than English. CCRPC/CUUATS has a significant number of LEP residents within the MPA but historically the frequency of contact with the MPO has been low. 

{{<image src="Translation availability RPC Website.PNG"
  alt="Screenshot of the CCRPC website with the Translate button selected and menu of language options available for users."
  caption="CCRPC website with the Translate button selected and menu of language options available for users."
  attr="CCRPC" attrlink="https://www.ccrpc.org/index.php"
  position="left">}}

Current resources available to the MPO do include the CCRPC website, which can provide translation into a broad range of languages (see picture provided). These translations are not available for web plan documents. Some documents can be made available upon request, but costs may be a factor in the final decision. Additionally, resources like Google Forms can allow users to directly translate surveys as well, which has been valuable for the development of the most recent Long Range Transportation Plan. Lastly, CCRPC provides access the Propio Language Services, which can provide video and phone interpreting on an as-needed basis. CCRPC/CUUATS remains committed to the principle of inclusivity and has used cost-effective means of multi-lingual outreach at important junctures in the planning process.



